package com.wsb.rozproszone.sklep.mapper;

import com.wsb.rozproszone.sklep.dto.ProductDto;
import com.wsb.rozproszone.sklep.persistance.model.Product;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ProductMapper {

    public static Product map(ProductDto dto){
        Product product = new Product();
        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        product.setDescription(dto.getDescription());
        return product;
    }

    public static ProductDto map(Product product){
        ProductDto dto = new ProductDto();
        dto.setId(product.getId());
        dto.setName(product.getName());
        dto.setPrice(product.getPrice());
        dto.setDescription(product.getDescription());
        dto.setSellerName(product.getUser().getName());
        return dto;
    }
}
