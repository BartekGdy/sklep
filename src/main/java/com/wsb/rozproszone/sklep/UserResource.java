package com.wsb.rozproszone.sklep;

import com.wsb.rozproszone.sklep.authentication.Token;
import com.wsb.rozproszone.sklep.authentication.TokenAuthenticationService;
import com.wsb.rozproszone.sklep.dto.Credentials;
import com.wsb.rozproszone.sklep.persistance.model.AppUser;
import com.wsb.rozproszone.sklep.persistance.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import static com.wsb.rozproszone.sklep.utils.AuthUtils.encode;
import static org.springframework.http.HttpStatus.CREATED;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserResource {

    private final UserRepository userRepository;
    private final TokenAuthenticationService tokenService;

    @PostMapping
    @ResponseStatus(CREATED)
    public void register(@RequestBody AppUser user) {
        String pass = encode(user.getPassword());
        user.setPassword(pass);
        userRepository.save(user);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, pass);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @PostMapping("/login")
    public ResponseEntity<Token> login(@RequestBody Credentials credentials) {
        String accessToken = tokenService
                .authenticate(new UsernamePasswordAuthenticationToken(credentials.getLogin(), credentials.getPassword()));
        Token token = new Token(accessToken);
        if (StringUtils.isEmpty(token)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(token, HttpStatus.OK);    }
}
