package com.wsb.rozproszone.sklep.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class ProductNotFound extends RuntimeException {

    public ProductNotFound() {
        super("Product not found");
    }

    public ProductNotFound(Long productId) {
        super(String.format("Product with id: %s Not Found", productId));
    }
}
