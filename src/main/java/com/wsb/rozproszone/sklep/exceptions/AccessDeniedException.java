package com.wsb.rozproszone.sklep.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.FORBIDDEN;

@ResponseStatus(FORBIDDEN)
public class AccessDeniedException extends RuntimeException{

    public AccessDeniedException() {
        this("Access Denied");
    }

    public AccessDeniedException(String message) {
        super(message);
    }
}
