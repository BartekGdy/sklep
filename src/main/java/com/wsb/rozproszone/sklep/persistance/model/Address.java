package com.wsb.rozproszone.sklep.persistance.model;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Address {
    private String premise;
    private String street;
    private String postCode;
    private String city;
    private String country;
}
