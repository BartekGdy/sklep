package com.wsb.rozproszone.sklep.bootstrap;


import com.wsb.rozproszone.sklep.persistance.model.AppUser;
import com.wsb.rozproszone.sklep.persistance.model.Product;
import com.wsb.rozproszone.sklep.persistance.repository.ProductRepository;
import com.wsb.rozproszone.sklep.persistance.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class BootstrapDB implements CommandLineRunner{

    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        AppUser appUser = new AppUser();
        appUser.setLogin("Mock");
        appUser.setPassword("Mock");
        appUser.setName("Joe");
        appUser.setSurname("Doe");

        userRepository.saveAndFlush(appUser);

        Product product1 = new Product();
        product1.setName("Parasol");
        product1.setDescription("Niebieski. Garażowany");
        product1.setPrice(23.2);
        product1.setUser(appUser);

        Product product2 = new Product();
        product2.setName("Golf 2");
        product2.setDescription("Niemiec płakał jak sprzedawał");
        product2.setPrice(1548.99);
        product2.setUser(appUser);

        productRepository.saveAndFlush(product1);
        productRepository.saveAndFlush(product2);
    }
}
