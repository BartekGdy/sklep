package com.wsb.rozproszone.sklep.utils;

import lombok.experimental.UtilityClass;

import java.util.Base64;

@UtilityClass
public class AuthUtils {

    public static String encode(String s) {
        if (s == null) {
            return null;
        }
        byte[] coded = Base64.getEncoder().encode(s.getBytes());
        return new String(coded);
    }

    public static String decode(String s) {
        if (s == null) {
            return null;
        }
        byte[] decoded = Base64.getDecoder().decode(s);
        return new String(decoded);
    }
}
