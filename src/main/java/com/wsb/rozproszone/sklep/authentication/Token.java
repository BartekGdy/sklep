package com.wsb.rozproszone.sklep.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Token {

   private String accessToken;
}
