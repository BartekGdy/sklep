package com.wsb.rozproszone.sklep.authentication;

import com.wsb.rozproszone.sklep.persistance.model.AppUser;
import com.wsb.rozproszone.sklep.persistance.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;
import java.util.Optional;

import static com.wsb.rozproszone.sklep.utils.AuthUtils.decode;
import static com.wsb.rozproszone.sklep.utils.AuthUtils.encode;
import static java.util.Collections.singletonList;

@Service
@Slf4j
@RequiredArgsConstructor
public class TokenAuthenticationService {

    public static final String AUTH_HEADER_NAME = "Authorization";
    public static final String API_KEY_HEADER_NAME = "api_key";
    public static final String MAIN_JWT_HEADER_NAME = "mainJwt";
    private final UserRepository userRepository;
    private final UserDetailsChecker userDetailsChecker;


    public Authentication getAuthentication(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        Optional<String> currentUserToken = this.getCurrentUserToken(httpRequest);
        if (currentUserToken.isPresent()) {
            UserDetails user = this.parseToken((String) currentUserToken.get());
            this.userDetailsChecker.check(user);
            return new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities());
        } else {
            log.warn("Auth Token was not found in api_key nor in Authorization");
        }
        return null;
    }

    private UserDetails parseToken(String token) {
        String[] credentials = token.split("@");
        if (credentials.length != 2) {
            credentials = new String[]{null, null};
        }
        String login = credentials[0];
        String password = credentials[1];

        return new User(login, password, true, true, true, true, singletonList(new SimpleGrantedAuthority("USER")));
    }


    private Optional<String> getCurrentUserToken(HttpServletRequest request) {
        String token = request.getHeader(API_KEY_HEADER_NAME);
        if (StringUtils.isEmpty(token)) {
            token = request.getHeader(AUTH_HEADER_NAME);
        }
        token = decode(token);
        return StringUtils.isEmpty(token) ? Optional.empty() : Optional.of(token);
    }

    public String authenticate(Authentication authentication) throws AuthenticationException {
        String login = (String) authentication.getPrincipal();
        UserDetails user = getUser(login);
        if (authentication.getCredentials() == null) {
            throw new HTTPException(401);
        } else if (!authentication.getCredentials().equals(user.getPassword())) {
            throw new HTTPException(401);
        } else {
            this.userDetailsChecker.check(user);
            return encode(this.createTokenForUser(user));
        }
    }

    private User getUser(String login) {
        AppUser appUser = userRepository.findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid login"));
        return new User(appUser.getLogin(), decode(appUser.getPassword()), true, true, true, true, singletonList(new SimpleGrantedAuthority("USER")));
    }

    private String createTokenForUser(UserDetails user) {
        return user.getUsername() + "@" + user.getPassword();
    }

    public String getUserNameFromToken(final HttpServletRequest request) {
        Optional<String> currentUserToken = getCurrentUserToken(request);
        if (!currentUserToken.isPresent()) {
            throw new AuthenticationCredentialsNotFoundException("You are not authorized to this request");
        }
        return parseToken(currentUserToken.get()).getUsername();
    }
}