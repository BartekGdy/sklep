package com.wsb.rozproszone.sklep;

import com.wsb.rozproszone.sklep.authentication.TokenAuthenticationService;
import com.wsb.rozproszone.sklep.dto.ProductDto;
import com.wsb.rozproszone.sklep.exceptions.AccessDeniedException;
import com.wsb.rozproszone.sklep.exceptions.ProductNotFound;
import com.wsb.rozproszone.sklep.mapper.ProductMapper;
import com.wsb.rozproszone.sklep.persistance.model.AppUser;
import com.wsb.rozproszone.sklep.persistance.model.Product;
import com.wsb.rozproszone.sklep.persistance.repository.ProductRepository;
import com.wsb.rozproszone.sklep.persistance.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

import static com.wsb.rozproszone.sklep.mapper.ProductMapper.map;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequiredArgsConstructor
@RequestMapping("/offer")
public class OfferResource {

    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final TokenAuthenticationService tokenAuthenticationService;

    @PostMapping
    @ResponseStatus(CREATED)
    public ProductDto addProduct(@RequestBody ProductDto productDto, HttpServletRequest request) {
        AppUser user = getAppUser(request);
        Product product = map(productDto);
        product.setTime(LocalDateTime.now().withNano(0));
        product.setUser(user);
        return map(productRepository.save(product));
    }

    @GetMapping
    public List<ProductDto> getProducts() {
        return productRepository.findAll().stream()
                .map(ProductMapper::map)
                .collect(toList());
    }

    @GetMapping("/{id}")
    public ProductDto getProduct(@PathVariable("id") Long id) {
        return productRepository.findById(id)
                .map(ProductMapper::map)
                .orElseThrow(() -> new ProductNotFound(id));
    }

    @PutMapping("/{id}")
    public ProductDto alterProduct(@PathVariable("id") Long id, @RequestBody Product newProduct, HttpServletRequest request) {
        Product product = productRepository.findById(id)
                .map(oldProduct -> updateProduct(oldProduct, newProduct))
                .map(productRepository::save)
                .orElseThrow(ProductNotFound::new);

        if (!getAppUser(request).getId().equals(product.getUser().getId())) {
            throw new AccessDeniedException("Access Denied");
        }
        return map(product);
    }

    @DeleteMapping("delete/{id}")
    public void deleteProduct(@PathVariable("id") Long id, HttpServletRequest request) {
        Product product = productRepository.findById(id)
                .orElseThrow(ProductNotFound::new);

        if (!getAppUser(request).getId().equals(product.getUser().getId())) {
            throw new AccessDeniedException("Access Denied");
        }
        productRepository.delete(product);
    }

    private AppUser getAppUser(HttpServletRequest request) {
        String login = tokenAuthenticationService.getUserNameFromToken(request);
        return userRepository.findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("You need to be logged in to add products"));
    }

    private Product updateProduct(Product oldProduct, Product newProduct) {
        oldProduct.setName(newProduct.getName());
        oldProduct.setPrice(newProduct.getPrice());
        return oldProduct;
    }
}
