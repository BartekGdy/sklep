package com.wsb.rozproszone.sklep.dto;

import lombok.Data;

@Data
public class ProductDto {

    private Long id;
    private String name;
    private String sellerName;
    private String description;
    private Double price;
}
